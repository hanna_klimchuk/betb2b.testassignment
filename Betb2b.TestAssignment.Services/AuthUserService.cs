﻿using System.Threading.Tasks;
using AutoMapper;
using Betb2b.TestAssignment.Repository.Interfaces;
using Betb2b.TestAssignment.Services.Interfaces;

namespace Betb2b.TestAssignment.Services
{
    public class AuthUserService : IAuthUserService
    {
        private readonly IAuthUserRepository _authUserRepository;
        private readonly IMapper _mapper;

        public AuthUserService(IAuthUserRepository authUserRepository, IMapper mapper)
        {
            _authUserRepository = authUserRepository;
            _mapper = mapper;
        }

        public async Task<bool> IsValidUser(string userName, string password)
        {
            var user = await _authUserRepository.GetBy(userName, password);
            return user != null;
        }
    }
}
