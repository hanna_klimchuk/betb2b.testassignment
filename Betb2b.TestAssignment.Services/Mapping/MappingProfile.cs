﻿using AutoMapper;
using Betb2b.TestAssignment.Core.Models.Requests;
using IM = Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateUserRequest, IM.User>()
                .ForMember(c => c.Id, opt => opt.MapFrom(u => u.User.Id))
                .ForMember(c => c.Name, opt => opt.MapFrom(u => u.User.Name))
                .ForMember(c => c.Status, opt => opt.MapFrom(u => u.User.Status));

            CreateMap<IM.User, UserElement>()
                .ForMember(c => c.Id, opt => opt.MapFrom(u => u.Id))
                .ForMember(c => c.Name, opt => opt.MapFrom(u => u.Name))
                .ForMember(c => c.Status, opt => opt.MapFrom(u => u.Status));
        }
    }
}
