﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Betb2b.TestAssignment.Core.Interfaces;
using Betb2b.TestAssignment.Core.Models.Requests;
using Betb2b.TestAssignment.IntermediateModel;
using Betb2b.TestAssignment.Repository.Interfaces;
using Betb2b.TestAssignment.Services.Interfaces;
using Newtonsoft.Json;
using IDatabase = StackExchange.Redis.IDatabase;

namespace Betb2b.TestAssignment.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IDatabase _cache;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository,
            ICacheManager cacheManager,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _cache = cacheManager.GetDatabase();
            _mapper = mapper;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userRepository.GetAllAsync();
        }

        public async Task<User> GetById(int id)
        {
           var cachedUser = await _cache.StringGetAsync(id.ToString());
           return cachedUser.HasValue ? JsonConvert.DeserializeObject<User>(cachedUser) : null;
        }

        public async Task<CreateUserResponse> Create(CreateUserRequest request)
        {
            var existingUser = await _userRepository.GetActiveById(request.User.Id);
            if (existingUser == null)
            {
                var user = _mapper.Map<User>(request);
                await _userRepository.Add(user);

                return new CreateUserResponse()
                {
                    Success = true,
                    User = _mapper.Map<UserElement>(user)
                };
            }

            return new CreateUserResponse()
            {
                ErrorId = 1,
                ErrorMsg = $"User with id {request.User.Id} already exist",
                Success = false
            };
        }

        public async Task<RemoveUserResponse> Remove(int id)
        {
            var existingUser = await _userRepository.GetActiveById(id);
            if (existingUser == null)
            {
                return new RemoveUserResponse()
                {
                    ErrorId = 2,
                    Msg = "User not found",
                    Success = false
                };
            }

            var resultUser = await _userRepository.Remove(existingUser);
            return new RemoveUserResponse()
            {
                Success = true,
                Msg = "User was removed",
                User = _mapper.Map<UserElement>(resultUser)
            };
        }

        public async Task<SetStatusResponse> SetStatus(SetStatusRequest request)
        {
            var existingUser = await _userRepository.GetActiveById(request.Id);
            if (existingUser == null)
            {
                return new SetStatusResponse()
                {
                    ErrorMsg = "User not found"
                };
            }

            var resultUser = await _userRepository.SetStatus(existingUser, request.NewStatus);
            return new SetStatusResponse()
            {
                User = _mapper.Map<UserElement>(resultUser)
            };
        }
    }
}
