﻿using System.Threading.Tasks;

namespace Betb2b.TestAssignment.Services.Interfaces
{
    public interface IAuthUserService
    {
        Task<bool> IsValidUser(string userName, string password);
    }
}
