﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Betb2b.TestAssignment.Core.Models.Requests;
using Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> GetById(int id);
        Task<CreateUserResponse> Create(CreateUserRequest request);
        Task<RemoveUserResponse> Remove(int id);
        Task<SetStatusResponse> SetStatus(SetStatusRequest request);
    }
}
