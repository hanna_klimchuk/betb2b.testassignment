﻿using System.Threading.Tasks;
using Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Repository.Interfaces
{
    public interface IAuthUserRepository
    {
        Task<AuthUser> GetBy(string name, string password);
    }
}
