﻿using System.Threading.Tasks;
using Betb2b.TestAssignment.Core.Models;
using Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<User[]> GetAllAsync();
        Task<User> Add(User user);
        Task<User> Remove(User user);
        Task<User> GetActiveById(int id);
        Task<User> SetStatus(User user, Status newStatus);
    }
}
