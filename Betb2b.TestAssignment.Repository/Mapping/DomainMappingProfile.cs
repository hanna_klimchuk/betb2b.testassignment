﻿using AutoMapper;
using Betb2b.TestAssignment.EntityFramework;
using IM = Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Repository.Mapping
{
    public class DomainMappingProfile : Profile
    {
        public DomainMappingProfile()
        {
            CreateMap<IM.User, User>().ReverseMap();
            CreateMap<AuthUser, IM.AuthUser>()
                .ForMember(c => c.Password, opt => opt.Ignore());
        }
    }
}
