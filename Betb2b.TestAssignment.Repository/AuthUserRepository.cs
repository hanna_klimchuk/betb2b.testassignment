﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Betb2b.TestAssignment.EntityFramework;
using Betb2b.TestAssignment.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using IM = Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Repository
{
    public class AuthUserRepository : IAuthUserRepository
    {
        private readonly TestAssignmentDbContext _dbContext;
        private readonly IMapper _mapper;

        public AuthUserRepository(TestAssignmentDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public void Dispose() => _dbContext.Dispose();

        public async Task<IM.AuthUser> GetBy(string name, string password)
        {
            var user = await _dbContext.AuthUsers
                .Where(u => u.Name == name && u.Password == password)
                .FirstOrDefaultAsync();

            return _mapper.Map<IM.AuthUser>(user);
        }
    }
}
