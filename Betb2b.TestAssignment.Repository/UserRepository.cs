﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Betb2b.TestAssignment.Core.Models;
using Betb2b.TestAssignment.EntityFramework;
using Betb2b.TestAssignment.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using IM = Betb2b.TestAssignment.IntermediateModel;

namespace Betb2b.TestAssignment.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly TestAssignmentDbContext _dbContext;
        private readonly IMapper _mapper;

        public UserRepository(TestAssignmentDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public void Dispose() => _dbContext.Dispose();

        public async Task<IM.User[]> GetAllAsync()
        {
            var dbUsers = await _dbContext.Users.AsNoTracking().ToArrayAsync();
            return _mapper.Map<IM.User[]>(dbUsers);
        }

        public async Task<IM.User> Add(IM.User user)
        {
            await _dbContext.Users.AddAsync(_mapper.Map<User>(user));
            await _dbContext.SaveChangesAsync();

            return user;
        }

        public async Task<IM.User> Remove(IM.User user)
        {
            return await SetStatus(user, Status.Deleted);
        }

        public async Task<IM.User> SetStatus(IM.User user, Status newStatus)
        {
            user.Status = newStatus;

            var entityUser = _mapper.Map<User>(user);
            _dbContext.Users.Update(entityUser);
            await _dbContext.SaveChangesAsync();

            return user;
        }

        public async Task<IM.User> GetActiveById(int id)
        {
            var dbUser = await _dbContext
                .Users.AsNoTracking()
                .Where(u => u.Id == id && u.Status != Status.Deleted)
                .FirstOrDefaultAsync();

            return _mapper.Map<IM.User>(dbUser);
        }
    }
}
