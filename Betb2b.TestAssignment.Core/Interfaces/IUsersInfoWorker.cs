﻿using System.Threading;
using System.Threading.Tasks;

namespace Betb2b.TestAssignment.Core.Interfaces
{
    public interface IUsersInfoWorker
    {
        Task Load(CancellationToken cancellationToken);
    }
}
