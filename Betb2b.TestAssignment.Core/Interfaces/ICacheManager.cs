﻿using System.Net;
using StackExchange.Redis;

namespace Betb2b.TestAssignment.Core.Interfaces
{
    public interface ICacheManager
    {
        IDatabase GetDatabase();
        EndPoint[] GetEndPoints();
        IServer GetServer(string host, int port);
    }
}
