﻿namespace Betb2b.TestAssignment.Core.Models
{
    public enum Status
    {
        New = 0,
        Active,
        Blocked,
        Deleted
    }
}
