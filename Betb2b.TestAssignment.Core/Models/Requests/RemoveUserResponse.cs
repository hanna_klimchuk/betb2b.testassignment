﻿namespace Betb2b.TestAssignment.Core.Models.Requests
{
    public class RemoveUserResponse
    {
        public UserElement User { get; set; }
        public bool Success { get; set; }
        public string Msg { get; set; }
        public int? ErrorId { get; set; }
    }
}
