﻿using System.ComponentModel.DataAnnotations;

namespace Betb2b.TestAssignment.Core.Models.Requests
{
    public class SetStatusRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public Status NewStatus { get; set; }
    }
}
