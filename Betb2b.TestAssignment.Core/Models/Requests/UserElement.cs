﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Betb2b.TestAssignment.Core.Models.Requests
{
    public class UserElement
    {
        [XmlAttribute]
        public int Id { get; set; }

        [XmlAttribute]
        [Required]
        public string Name { get; set; }

        [XmlElement("Status")]
        public Status Status { get; set; }
    }
}
