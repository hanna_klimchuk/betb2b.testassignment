﻿using System.Xml.Serialization;

namespace Betb2b.TestAssignment.Core.Models.Requests
{
    [XmlRoot("Response")]
    public class CreateUserResponse
    {
        [XmlAttribute]
        public bool Success { get; set; }

        [XmlAttribute]
        public byte ErrorId { get; set; }

        [XmlElement("user", IsNullable = false)]
        public UserElement User { get; set; }

        [XmlElement("ErrorMsg", IsNullable = false)]
        public string ErrorMsg { get; set; }
    }
}
