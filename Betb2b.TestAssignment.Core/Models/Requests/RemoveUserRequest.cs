﻿using System.ComponentModel.DataAnnotations;

namespace Betb2b.TestAssignment.Core.Models.Requests
{
    public class RemoveUserRequest
    {
        [Required]
        public RemoveUser RemoveUser { get; set; }
    }

    public class RemoveUser
    {
        [Required]
        public int Id { get; set; }
    }
}
