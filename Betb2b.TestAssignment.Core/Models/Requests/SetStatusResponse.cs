﻿namespace Betb2b.TestAssignment.Core.Models.Requests
{
    public class SetStatusResponse
    {
        public UserElement User { get; set; }
        public string ErrorMsg { get; set; }
    }
}
