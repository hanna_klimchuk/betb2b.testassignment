﻿using System.Xml.Serialization;

namespace Betb2b.TestAssignment.Core.Models.Requests
{
    [XmlRoot("Request")]
    public class CreateUserRequest
    {
        [XmlElement("user")]
        public UserElement User { get; set; }
    }
}
