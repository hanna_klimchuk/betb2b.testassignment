﻿namespace Betb2b.TestAssignment.Core.Options
{
    public class ConnectionStringsOptions
    {
        public const string ConnectionStrings = "ConnectionStrings";
        public string TestAssignmentDb { get; set; }
        public string RedisCache { get; set; }
    }
}
