# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an implementation of the test assignment from Betb2b company.

### Dependencies ###

I do not have MySQL Server installed on my machine so I decided to use Azure capabilities for hosting database. Taking to the account the requirement to avoid usage of MemoryCache I decided to go with distributed caching. And for this purpose I have chosen Azure Redis database. All Azure resources are hosted under my personal subscription. Appsettings.Development.json file contains the full connection strings. I know that it is not safe but you can run the application as well.

* Azure Database for MySQL server
  mysql --host=testassignmentcyprus.mysql.database.azure.com --user=testadmin@testassignmentcyprus -p
  password=YZWcJj252pc5QNQ

* Azure Redis cache (testassignment.redis.cache.windows.net)

  Database contains the following data for testing purposes.
  
  For basic authorization test:
  INSERT INTO AuthUsers (Name, Password) VALUES ("TestUser", "TestUser12345"); 
  
  For user info test:
  (Id, Name, Status) - (1000, "Hanna", 0) - (1001, "Test", 1)

### How do I get set up? ###

* Clone #develop# branch
* Build and run the application Betb2b.TestAssignment.WebApi in Visual Studio
* Build and run the application Betb2b.TestAssignment.TestConsole in Visual Studio in a new instance
* Check output in console
* Use Postman for more calls