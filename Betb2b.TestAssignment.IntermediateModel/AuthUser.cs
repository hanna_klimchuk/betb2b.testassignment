﻿namespace Betb2b.TestAssignment.IntermediateModel
{
    public class AuthUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
