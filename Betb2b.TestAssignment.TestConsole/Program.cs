﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Betb2b.TestAssignment.TestConsole
{
    class Program : IDisposable
    {
        private static readonly HttpClient client;
        private static readonly int userId;
        private static readonly string userCredentials = "TestUser:TestUser12345";

        static Program()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/user/");

            userId = new Random().Next(2000);
        }

        static async Task Main(string[] args)
        {
            await WrongAuth();

            await CreateUser();
            await CreateUser();

            await SetStatus();

            await RemoveUser();
            await RemoveUser();

            await GetUserInfo();
            await GetUserInfo(1000);

            Console.ReadLine();
        }

        private static async Task WrongAuth()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

            AddAuthHeader("TestUser:TestUser1234");

            var parameters = new Dictionary<string, string>
            {
                { "Id", "999" },
                { "NewStatus", "Blocked" }
            };

            var response = await client.PostAsync("Auth/SetStatus", new FormUrlEncodedContent(parameters));

            await PrintResult(response, $"Login with wrong password");
        }

        private static async Task CreateUser()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

            AddAuthHeader();

            var requestStr = "<Request><user Id=\"" + userId + "\" Name=\"alex\"><Status>New</Status></user></Request>";
            var requestContent = new StringContent(requestStr, Encoding.UTF8, "application/xml");
            var response = await client.PostAsync("Auth/CreateUser", requestContent);

            if (response.IsSuccessStatusCode)
            {
                await PrintResult(response, $"Create user with id {userId}");
            }
        }

        private static async Task SetStatus()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

            AddAuthHeader();

            var parameters = new Dictionary<string, string>();
            parameters.Add("Id", userId.ToString());
            parameters.Add("NewStatus", "Blocked");

            var response = await client.PostAsync("Auth/SetStatus", new FormUrlEncodedContent(parameters));
            if (response.IsSuccessStatusCode)
            {
                await PrintResult(response, $"Set Status=Blocked for user with id {userId}");
            }
        }

        private static async Task RemoveUser()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AddAuthHeader();

            var requestStr = "{\"RemoveUser\":{\"Id\":" + userId + "}}";
            var requestContent = new StringContent(requestStr, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("Auth/RemoveUser", requestContent);
            if (response.IsSuccessStatusCode)
            {
                if (response.IsSuccessStatusCode)
                {
                    await PrintResult(response, $"Remove user with id {userId}");
                }
            }
        }

        private static async Task GetUserInfo(int? id = null)
        {
            id ??= userId;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));

            var uri = "public/UserInfo?id=" + id;
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                await PrintResult(response, $"Get user info with id {id}");
            }
        }

        private static void AddAuthHeader(string credentials = null)
        {
            credentials ??= userCredentials;
            var byteArray = Encoding.ASCII.GetBytes(credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

        private static async Task PrintResult(HttpResponseMessage response, string title)
        {
            var responseContent = await response.Content.ReadAsStringAsync();

            Console.WriteLine(title);
            Console.WriteLine("---Response---");
            Console.WriteLine($"Status code: {(int)response.StatusCode}");
            Console.WriteLine($"Content: {responseContent}");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }

        public void Dispose()
        {
            client?.Dispose();
        }
    }
}
