﻿using System.Threading;
using System.Threading.Tasks;
using Betb2b.TestAssignment.Core.Interfaces;
using Betb2b.TestAssignment.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Betb2b.TestAssignment.WebApi.Infrastructure
{
    public class UsersInfoWorker : IUsersInfoWorker
    {
        private readonly ILogger<UsersInfoWorker> _logger;
        private readonly IUserService _userService;
        private readonly ICacheManager _cacheManager;

        public UsersInfoWorker(ILogger<UsersInfoWorker> logger,
            IUserService userService,
            ICacheManager cacheManager)
        {
            _logger = logger;
            _userService = userService;
            _cacheManager = cacheManager;
        }

        public async Task Load(CancellationToken cancellationToken)
        {
            var users = await _userService.GetAll();

            var cache = _cacheManager.GetDatabase();
            foreach (var user in users)
            {
                await cache.StringSetAsync(user.Id.ToString(), JsonConvert.SerializeObject(user));
            }

            _logger.LogInformation("UsersInfoWorker loaded information to Redis cache");
        }
    }
}
