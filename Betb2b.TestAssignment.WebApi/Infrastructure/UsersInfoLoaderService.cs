﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Betb2b.TestAssignment.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Betb2b.TestAssignment.WebApi.Infrastructure
{
    public class UsersInfoLoaderService : IHostedService
    {
        private readonly ILogger<UsersInfoLoaderService> _logger;
        private readonly IServiceProvider _serviceProvider;

        private static TimeSpan interval = TimeSpan.FromMinutes(10);

        public UsersInfoLoaderService(ILogger<UsersInfoLoaderService> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("UsersInfoLoader service is running.");

            await RepeatLoadingEvery(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("UsersInfoLoader service is stopping.");

            return Task.CompletedTask;
        }

        private async Task RepeatLoadingEvery(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();
            var worker = scope.ServiceProvider.GetRequiredService<IUsersInfoWorker>();

            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                    break;

                await worker.Load(cancellationToken);
                var task = Task.Delay(interval, cancellationToken);

                try
                {
                    await task;
                }
                catch (TaskCanceledException)
                {
                    return;
                }
            }
        }
    }
}
