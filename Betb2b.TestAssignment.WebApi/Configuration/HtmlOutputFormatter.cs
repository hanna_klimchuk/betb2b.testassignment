﻿using Microsoft.AspNetCore.Mvc.Formatters;

namespace Betb2b.TestAssignment.WebApi.Configuration
{
    public class HtmlOutputFormatter : StringOutputFormatter
    {
        public HtmlOutputFormatter()
        {
            SupportedMediaTypes.Add("text/html");
        }
    }
}
