using AutoMapper;
using Betb2b.TestAssignment.Core.Infrastructure;
using Betb2b.TestAssignment.Core.Interfaces;
using Betb2b.TestAssignment.Core.Options;
using Betb2b.TestAssignment.EntityFramework;
using Betb2b.TestAssignment.Repository;
using Betb2b.TestAssignment.Repository.Interfaces;
using Betb2b.TestAssignment.Repository.Mapping;
using Betb2b.TestAssignment.Services;
using Betb2b.TestAssignment.Services.Interfaces;
using Betb2b.TestAssignment.Services.Mapping;
using Betb2b.TestAssignment.WebApi.Configuration;
using Betb2b.TestAssignment.WebApi.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Betb2b.TestAssignment.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                    builder.SetIsOriginAllowed(_ => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            var connectionStringsOptions = new ConnectionStringsOptions();
            _configuration.GetSection(ConnectionStringsOptions.ConnectionStrings).Bind(connectionStringsOptions);
            services.AddSingleton(connectionStringsOptions);

            services.AddDbContextPool<TestAssignmentDbContext>(
                options => options.UseMySql(connectionStringsOptions.TestAssignmentDb, ServerVersion.AutoDetect(connectionStringsOptions.TestAssignmentDb))
            );

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
                mc.AddProfile(new DomainMappingProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services
                .AddControllers(options =>
                {
                    options.OutputFormatters.Add(new XmlSerializerOutputFormatterNamespace());
                    options.OutputFormatters.Add(new HtmlOutputFormatter());
                })
                .AddXmlSerializerFormatters()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            services.AddSingleton<ICacheManager, CacheManager>();
            services.AddScoped<IUsersInfoWorker, UsersInfoWorker>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthUserService, AuthUserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAuthUserRepository, AuthUserRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
