﻿using Microsoft.AspNetCore.Mvc;

namespace Betb2b.TestAssignment.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Hello!");
        }
    }
}
