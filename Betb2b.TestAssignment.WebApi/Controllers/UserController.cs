﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Betb2b.TestAssignment.Core.Models.Requests;
using Betb2b.TestAssignment.Services.Interfaces;
using Betb2b.TestAssignment.WebApi.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Betb2b.TestAssignment.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Auth/CreateUser")]
        [Produces("application/xml")]
        [BasicAuth]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserRequest request)
        {
            var response = await _userService.Create(request);
            return Ok(response);
        }

        [HttpPost("Auth/RemoveUser")]
        [Produces("application/json")]
        [BasicAuth]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RemoveUser([FromBody] RemoveUserRequest request)
        {
            var response = await _userService.Remove(request.RemoveUser.Id);
            return Ok(response);
        }

        [HttpGet("Public/UserInfo")]
        [Produces("text/html")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UserInfo([FromQuery, Required] int id)
        {
            var userInfo = await _userService.GetById(id);
            var htmlTemplate = userInfo == null
                ? $"<b>No information for user id={id}</b>"
                : $"<b>User information:</b> </br><p>Id: {id} </br>Name: {userInfo.Name} </br>Status: {userInfo.Status}</p>";

            return Ok(htmlTemplate);
        }

        [HttpPost("Auth/SetStatus")]
        [Consumes("application/x-www-form-urlencoded")]
        [Produces("application/json")]
        [BasicAuth]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SetStatus([FromForm] SetStatusRequest request)
        {
            var response = await _userService.SetStatus(request);
            return Ok(response);
        }
    }
}
