﻿namespace Betb2b.TestAssignment.EntityFramework
{
    public class AuthUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
