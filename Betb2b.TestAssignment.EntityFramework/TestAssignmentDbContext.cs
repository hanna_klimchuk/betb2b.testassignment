﻿using Microsoft.EntityFrameworkCore;

namespace Betb2b.TestAssignment.EntityFramework
{
    public class TestAssignmentDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<AuthUser> AuthUsers { get; set; }

        public TestAssignmentDbContext(DbContextOptions<TestAssignmentDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users");

                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();

                entity.Property(e => e.Name).HasColumnType("nvarchar(50)").IsRequired();
                entity.Property(e => e.Status).HasColumnType("tinyint unsigned").IsRequired();
            });

            modelBuilder.Entity<AuthUser>(entity =>
            {
                entity.ToTable("AuthUsers");

                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();

                entity.Property(e => e.Name).HasColumnType("nvarchar(50)").IsRequired();
                entity.Property(e => e.Password).HasColumnType("nvarchar(50)").IsRequired();
            });
        }
    }
}
