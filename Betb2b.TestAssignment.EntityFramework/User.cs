﻿using Betb2b.TestAssignment.Core.Models;

namespace Betb2b.TestAssignment.EntityFramework
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Status Status { get; set; }
    }
}
